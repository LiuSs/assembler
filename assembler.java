/*
問題：
1.location的位址不是固定+3
2.應以「十六進位」處理
3.尚須載入opcode table
4.尚未切割每行的指令
*/

import java.io.*;
import java.util.*;

class assembler
{
	public static void main(String[] args)throws IOException			 //throws IOException 為字串讀取相關方法必用
	{
		FileReader fo = new FileReader("source.txt");					 //fo is mean file open
		BufferedReader br = new BufferedReader(fo);						 //BufferedReader is 讀取暫存器，將txt的文字全部讀取

		int i=0;														 //紀錄location指向第幾格
		int[] location = new int[100];									 //儲存location
		String[] input_SIC = new String[100];					 		 //以一維陣列儲存每行指令(視為原始輸入)
		String[][] SIC_split = new String[100][100];					 //將分割後的結果先以二維陣列儲存，其中列為整行指令，
																		 //而行分別儲存[標籤or變數名稱][指令or變數型態][指令使用標籤][逗號][X或其他功用]
		location[0] = 16384;											 //start address(暫時以10進位計算) 16384 = 4000十六進制
		String[] location_hex = new String[100];						 //存放16進制之值(以"String"的型別"
		location_hex[0] = String.format("%X",location[0]);
////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*以下開始進行讀取，並對讀入的指令處理*/
		while(br.ready())												 //藉由ready()方法檢查txt是否讀取完畢
		{
			input_SIC[i] = br.readLine();								 //readLine()方法一次讀取一行
			String[] SIC_midsplit = input_SIC[i].split(" ");             //以空白作為分割單位，並暫時儲存在中繼陣列



			int k = 0,sw=0;

			for(int j=0;j<SIC_midsplit.length;j++)
			{

				if("".equals(SIC_midsplit[j]) && k==0)
				{
					sw=1;
					continue;
				}
				if(sw==1)
				{
					k++;sw=0;
				}
				if("".equals(SIC_midsplit[j]) && k>=1)
				{
					continue;
				}
				SIC_split[i][k] =SIC_midsplit[j];
				k++;

				//將二維陣列的行，儲存分割完成的指令
			}





			System.out.print(location_hex[i] + "　　　");
			for(int j=0;j<k;j++)
			{
				System.out.print("\t\t"+SIC_split[i][j]);					 // 印出來測試
			}
			System.out.println("");

			if("RESB".equals(SIC_midsplit[1]))
			{
				location[i+1] = location[0] + Integer.parseInt(SIC_midsplit[2]);
			}

			else
			{
				location[i+1] = location[0] + 3*i;							 //i+1指向下一個location
				i = i+1;
			}

			location_hex[i] = String.format("%X",location[i]);
		}


		fo.close();														 //使用完關閉檔案

////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*		FileWriter fw = new FileWriter("output.txt");//fw is mean file write in
		//fw.write("test");
		fw.flush();//清空 暫存空間
		fw.close();//使用完關閉當按
*/
	}
}

